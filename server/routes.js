// routes.js
// Jeris Jones
// March-April 2019
// Creates an Express server and defines routes for CRUD operations to the Firestore database

const express = require("express");
const router = express.Router();
const dbRtns = require("./dbRoutines");

const admin = require("firebase-admin");
const serviceAccount = require("./meta-be3a7-firebase-adminsdk-v8wsb-eb45be251a.json");

// Firestore initialization
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://meta-be3a7.firebaseio.com"
});

// ------- GET -------

// Get all project backlog stories
router.get("/projects/:uid/backlog", async (req, res) => {
  let db;
  try {
    db = admin.firestore();
    let project_UID = req.params.uid;
    let backlog = await dbRtns.GetProjectBacklog(db, project_UID);
    res.send(backlog);
  } catch (err) {
    console.log(`Error retrieving backlog - ${err.stack}`);
    res.status(500).send(`Error retrieving backlog - Internal Server Error`);
  }
});

// Get all sprint backlog stories
router.get("/projects/:uid/:sprint/backlog", async (req, res) => {
  let db;
  try {
    db = admin.firestore();
    let project_UID = req.params.uid;
    let sprintNum = req.params.sprint;
    let backlog = await dbRtns.GetSprintBacklog(db, project_UID, sprintNum);
    res.send(backlog);
  } catch (err) {
    console.log(`Error retrieving sprint backlog - ${err.stack}`);
    res
      .status(500)
      .send(`Error retrieving sprint backlog - Internal Server Error`);
  }
});

// Get project data
router.get("/projects/:uid", async (req, res) => {
  let db;
  try {
    db = admin.firestore();
    let project_UID = req.params.uid;
    let data = await dbRtns.GetProjectData(db, project_UID);
    res.send(data);
  } catch (err) {
    console.log(`Error retrieving project data - ${err.stack}`);
    res
      .status(500)
      .send(`Error retrieving project data - Internal Server Error`);
  }
});

// Get all projects for user
router.get("/users/projects/:user", async (req, res) => {
  let db;
  try {
    db = admin.firestore();
    let username = req.params.user;
    let results = await dbRtns.GetProjectsByUser(db, username);
    results === undefined
      ? res.status(400).send(`Error: user ${username} not found.`)
      : res.send(results);
  } catch (err) {
    console.log(`Error retrieving projects\n${err.stack}`);
    res.status(500).send(`Error retrieving projects - Internal Server Error`);
  }
});

// Get a user object json
router.get("/users/:user", async (req, res) => {
  let db;
  try {
    db = admin.firestore();
    let username = req.params.user;
    let user = await dbRtns.GetUser(db, username);
    console.log(user);
    user === undefined
      ? res.status(400).send(`Error: user ${username} not found.`)
      : res.send(user);
  } catch (err) {
    console.log(`Error retrieving user\n${err.stack}`);
    res.status(500).send(`Error retrieving user - Internal Server Error`);
  }
});

// ------- POST -------
// Add a new user
router.post("/users", async (req, res) => {
  let db;
  try {
    db = admin.firestore();
    let user = req.body;
    let result = await dbRtns.AddUser(db, user);
    result === undefined
      ? res.status(400).send(`Error: user ${user.name} could not be added.`)
      : res.send(result);
  } catch (err) {
    console.log(`Error adding user - ${err.stack}`);
    res.status(500).send(`Error adding user - Internal Server Error`);
  }
});

// Add a project with document properties and corresponding amount of sprints
router.post('/projects', async (req, res) => {
  let db;
  try {
    db = admin.firestore();
    let project = req.body; // name, numSprints, members[]
    let result = await dbRtns.AddProject(db, project);
    result === undefined
      ? res.status(400).send(`Error - unable to add project`)
      : res.send(result);
  } catch (err) {
    console.log(`Error adding project - ${err.stack}`);
    res.status(500).send(`Internal Server Error while adding Project`);
  }
});

// ------- PUT -------

// Update a user (password and projects)
router.put("/users/:username", async (req, res) => {
  let db;
  try {
    db = admin.firestore();
    let username = req.params.username;
    let result = await dbRtns.UpdateUser(db, username, req.body);
    result === undefined
      ? res.status(400).send(`Error updating ${username}`)
      : res.send(result);
  } catch (err) {
    console.log(`Error updating user - ${err.stack}`);
    res.status(500).send(`Error updating user - Internal Server Error`);
  }
});

// Further functionality needed:
// PUT to update sprints and add stories
// PUT to update a project's fields

module.exports = router;
