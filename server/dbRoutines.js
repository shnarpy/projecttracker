// dbRoutines.js
// Jeris Jones
// March-April 2019
// All database operations with Google Firestore to be used in Express routes

// Return all projects for a user
exports.GetProjectsByUser = (db, username) => {
  let usersRef = db.collection("users").doc(username);
  let doc = usersRef
    .get()
    .then(doc => {
      if (doc.empty) {
        return undefined;
      }
      let data = doc.data().projects;
      return data;
    })
    .catch(function() {
      return undefined;
    });

  return doc;
};

// Return JSON for a user
exports.GetUser = (db, username) => {
  let usersRef = db.collection("users").doc(username);
  let userDoc = usersRef
    .get()
    .then(doc => {
      if (doc.empty) {
        return undefined;
      }
      return doc.data();
    })
    .catch(function() {
      return undefined;
    });

  return userDoc;
};

// Add a user to the users collection in firestore
exports.AddUser = (db, user) => {
  let result = db
    .collection("users")
    .doc(user.name)
    .set({
      password: user.password,
      projects: user.projects
    })
    .then(function() {
      return `Added user ${user.name}`;
    })
    .catch(function() {
      return undefined;
    });

  return result;
};

// Update a user's password or projects in firestore
exports.UpdateUser = (db, username, user) => {
  let userRef = db.collection("users").doc(username);
  let result = userRef
    .update({
      password: user.password,
      projects: user.projects
    })
    .then(function() {
      return `Updated user ${username}`;
    })
    .catch(function() {
      return undefined;
    });

  return result;
};

// Add a member to a project's list of members in firestore
exports.AddMemberToProject = (db, project_UID, username) => {
  let projRef = db.collection("projects").doc(project_UID);
  let result = projRef
    .set({ members: [username] }, { merge: true })
    .then(function() {
      return `Added ${username} to project ${project_UID}`;
    })
    .catch(function() {
      return undefined;
    });

  return result;
};

// Get the (icebox) backlog of all stories for a project
exports.GetProjectBacklog = (db, project_UID) => {
  let projRef = db
    .collection("projects")
    .doc(project_UID)
    .collection("backlog");

  let backlog = projRef
    .get()
    .then(snapshot => {
      let stories = [];
      snapshot.forEach(doc => {
        stories.push({ id: doc.id, data: doc.data() });
      });
      return stories;
    })
    .catch(err => {
      return `Error getting project backlog for ${project_UID}\n${err}`;
    });

  return backlog;
};

// Get the backlog of stories for a particular sprint
exports.GetSprintBacklog = (db, project_UID, sprintNum) => {
  let sprintRef = db
    .collection("projects")
    .doc(project_UID)
    .collection("sprints")
    .doc(sprintNum)
    .collection("backlog");

  let backlog = sprintRef
    .get()
    .then(snapshot => {
      let stories = [];
      snapshot.forEach(doc => {
        stories.push({ id: doc.id, data: doc.data() });
      });
      return stories;
    })
    .catch(err => {
      return `Error getting sprint backlog for sprint ${sprintNum}\n${err}`;
    });

  return backlog;
};

// Get all fields for a project (shallow - not the backlog or sprints)
exports.GetProjectData = (db, project_UID) => {
  let projectRef = db.collection("projects").doc(project_UID);

  let data = projectRef
    .get()
    .then(doc => {
      return { id: doc.id, data: doc.data() };
    })
    .catch(err => {
      return `Error getting project data for project ${project_UID}\n${err}`;
    });
  return data;
};

// Add a new project to the projects collection in firestore
exports.AddProject = async (db, project) => {
  // Add the base project doc
  let new_UID = await db
    .collection("projects")
    .add(project)
    .then(ref => {
      return ref.id;
    })
    .catch(function() {
      return undefined;
    });

  if (new_UID === undefined) return undefined;

  // // Add the subcollections
  let projRef = db.collection("projects").doc(new_UID);
  // Backlog
  let result = await projRef
    .collection("backlog")
    .add({
      description: "",
      estimate: 0
    })
    .then(function() {
      return "Added backlog\n";
    })
    .catch(function() {
      return undefined;
    });

  if (result === undefined) return undefined;

  // Sprints
  let numSprints = project.numsprints;
  for (let i = 1; i <= numSprints; ++i) {
    let sprintName = await projRef
      .collection("sprints")
      .doc(`Sprint ${i}`)
      .set({
        name: `Sprint ${i}`
      })
      .then(function() {
        return `Added sprint ${i}`;
      });

    // Add backlog to sprint
    let sprintBacklogResult = await projRef
      .collection("sprints")
      .doc(`Sprint ${i}`)
      .collection("backlog")
      .add({
        description: "",
        owner: "",
        startdate: "",
        completedate: null,
        estimate: 0,
        newestimate: 0,
        actual: 0,
        complete: false
      })
      .then(function() {
        return ` - backlog created\n`;
      });

    result += sprintName + sprintBacklogResult;
  }

  // return result;
  return { pID: new_UID };
};
