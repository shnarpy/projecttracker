const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const port = process.env.PORT || 5000;
const routes = require('./routes');

// app.use((req, res, next) => {
//   console.log('Time:', new Date() + 3600000 * -5.0); // gmt->est
//   next();
// });

// CORS 
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
  next();
});

// Parse application/json
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// Default route
app.use('/', routes);

app.use(express.static("public"));

app.use((err, req, res, next) => {
  console.error(err);
  res.status(500).send('internal server error');
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
