import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Card,
  CardContent,
  CardHeader,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Typography
} from "@material-ui/core";
import theme from "./theme";

class SelectedComponent extends React.PureComponent {
  state = { header: undefined, backlog: undefined };

  createTable = () => {
    if (this.state.backlog !== undefined) {
      return this.state.backlog.map(story => (
        <TableRow key={story.id}>
          <TableCell component="th" scope="row">
            {story.data.description}
          </TableCell>
          <TableCell align="right">{story.data.estimate}</TableCell>
          <TableCell>
            {/* <Button
              value={row.ID}
              onClick={this.selectPressed}
              color="primary"
              variant="contained"
              style={{ marginTop: 20, marginLeft: 20 }}
            >
              Select
            </Button> */}
          </TableCell>
        </TableRow>
      ));
    }
  };

  createHeader = () => {
    if (this.state.header !== undefined) {
      console.log(this.state.header);
      return (
        <div>
          <CardHeader
            title={this.state.header.data.name}
            color="primary"
            style={{ textAlign: "center" }}
          />
          <Typography>
            Sprint: {this.state.header.data.currentsprint}
          </Typography>
        </div>
      );
    }
  };

  render() {
    const { header } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <Card style={{ margin: 20 }}>
          {this.createHeader()}
          <Table>
            <TableHead>
              <TableRow>
                <TableCell align="center">description</TableCell>
                <TableCell align="center">Estimate</TableCell>
                <TableCell align="center">---</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{this.createTable()}</TableBody>
          </Table>
        </Card>
      </MuiThemeProvider>
    );
  }
  componentDidMount = async () => {
    console.log(this.props.project);

    try {
      let HeaderInfo = await fetch(
        `http://localhost:5000/projects/${this.props.project}`
      ).then(HeaderInfo => HeaderInfo.json());
      console.log(HeaderInfo);

      let Backlog = await fetch(
        `http://localhost:5000/projects/${this.props.project}/backlog`
      ).then(Backlog => Backlog.json());
      console.log(Backlog);

      this.setState({ header: HeaderInfo, backlog: Backlog });
    } catch (error) {
      console.log(error);
      this.setState({
        anchorEl: null
      });
    }
  };
}
export default SelectedComponent;
