import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";

class HomeComponent extends React.PureComponent {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <div className="App" align="center">
          <h1>HJK Project Tracker</h1>
          <img
            src={require("./images/agile-methodologies.png")}
            className="App-logo"
            height="200"
            alt="Logo Featuring Agile Development"
          />
        </div>
      </MuiThemeProvider>
    );
  }
}
export default HomeComponent;
