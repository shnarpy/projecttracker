import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  AppBar,
  Button,
  IconButton,
  Menu,
  MenuItem,
  TextField,
  Toolbar,
  Typography
} from "@material-ui/core";
import theme from "./theme";
import Reorder from "@material-ui/icons/Reorder";
import HomeComponent from "./HomeComponent";
import ProjectsComponent from "./ProjectComponent";
import SelectedComponent from "./SelectedComponent";

let projects = [];

class MenuComponent extends React.PureComponent {
  state = {
    anchorEl: null,
    showHome: true,
    showProjectsCard: false,
    showSelectedProject: false,
    selectedProject: "",
    loggedIn: false,
    username: "",
    password: "",
    smallView: false
  };
  onMenuItemClicked = event => {
    this.setState({ anchorEl: event.currentTarget });
  };
  onClose = () => {
    this.setState({ anchorEl: null });
  };
  onHomeItemClicked = () => {
    this.setState({
      showHome: true,
      showProjectsCard: false,
      anchorEl: null
    });
  };
  onProjectsItemClicked = () => {
    this.setState({
      showHome: false,
      showProjectsCard: true,
      anchorEl: null
    });
  };
  onUsernameChange = e => {
    this.setState({ username: e.target.value });
  };
  onPasswordChange = e => {
    this.setState({ password: e.target.value });
  };
  handleLogout = e => {
    this.setState({
      loggedIn: false,
      username: "",
      password: "",
      showHome: true,
      showProjectsCard: false,
      showSelectedProject: false
    });
    projects = [];
  };
  handleLogin = async () => {
    try {
      let response = await fetch(
        `http://localhost:5000/users/${this.state.username}`
      ).then(response => response.json());

      if (response.password === this.state.password) {
        this.setState({
          loggedIn: true,
          projects: response.projects,
          anchorE1: null
        });
      } else {
        this.setState({
          password: "",
          username: "",
          projects: [],
          anchorE1: null
        });
      }
    } catch (error) {
      console.log(error);
      this.setState({
        anchorEl: null
      });
    }
  };
  handleProjectSelected = data => {
    console.log(data);
    this.setState({
      showProjectsCard: false,
      showSelectedProject: true,
      selectedProject: data.Project
    });
  };
  resize() {
    let smallViewport = window.innerWidth <= 540;
    if (smallViewport !== this.state.smallView) {
      this.setState({ smallView: smallViewport });
    }
  }
  render() {
    const {
      anchorEl,
      showHome,
      showProjectsCard,
      showSelectedProject,
      loggedIn,
      username,
      password,
      smallView,
      projects,
      selectedProject
    } = this.state;
    const userEmptyorundefined = username === undefined || username === "";
    const passwordEmptyorundefined = password === undefined || password === "";
    const loginEmptyorundefined =
      userEmptyorundefined || passwordEmptyorundefined;
    return (
      <MuiThemeProvider theme={theme}>
        <AppBar position="static">
          <Toolbar>
            <IconButton onClick={this.onMenuItemClicked} color="inherit">
              <Reorder />
            </IconButton>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.onClose}
            >
              <MenuItem onClick={this.onHomeItemClicked}>Home</MenuItem>
              <MenuItem onClick={this.onProjectsItemClicked}>Projects</MenuItem>
            </Menu>
            <Typography variant="h6" color="inherit">
              HJK Project Tracker
            </Typography>
          </Toolbar>
        </AppBar>
        {/* LOGIN COMPONENT*/}
        {!loggedIn && (
          <div align="right">
            <TextField
              style={{ marginRight: 20 }}
              onChange={this.onUsernameChange}
              placeholder="Username"
              autoFocus={true}
              required
              value={username}
              error={userEmptyorundefined}
              helperText="Username Required"
            />
            <TextField
              style={{ marginRight: 20 }}
              onChange={this.onPasswordChange}
              placeholder="Password"
              value={password}
              error={passwordEmptyorundefined}
              helperText="Password Required"
            />
            {smallView && <br />}
            <Button
              onClick={this.handleLogin}
              color="primary"
              variant="contained"
              style={{ marginTop: "1em", marginRight: "1em" }}
              disabled={loginEmptyorundefined}
            >
              Login
            </Button>
          </div>
        )}
        {loggedIn && (
          <div align="right">
            <Button
              onClick={this.handleLogout}
              color="primary"
              variant="contained"
              style={{ marginTop: "1em", marginRight: "1em" }}
            >
              {this.state.username} Logout
            </Button>
          </div>
        )}
        {/* LOGIN COMPONENT END*/}
        {showHome && <HomeComponent />}
        {showProjectsCard && (
          <ProjectsComponent
            projects={projects}
            loggedIn={loggedIn}
            username={username}
            childdata={data => this.handleProjectSelected(data)}
          />
        )}
        {showSelectedProject && <SelectedComponent project={selectedProject} />}
      </MuiThemeProvider>
    );
  }
  componentDidMount() {
    window.addEventListener("resize", this.resize.bind(this));
    this.resize();
  }
}
export default MenuComponent;
