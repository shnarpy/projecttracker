import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Typography
} from "@material-ui/core";
import theme from "./theme";

function createData(ID, name, sprint) {
  return { ID, name, sprint };
}

class ProjectComponent extends React.PureComponent {
  state = {
    loggedIn: false,
    showNewCard: false,
    gotData: false,
    projectname: "",
    rows: undefined,
    projectIDs: [],
    data: []
  };

  ProjectSelect = rowData => {
    console.log(rowData);
  };

  newProjectButton = () => {
    this.setState({ showNewCard: true });
  };

  viewProjectsButton = () => {
    this.setState({ showNewCard: false });
  };

  createProject = async () => {
    let data = {
      name: this.state.projectname,
      currentsprint: 1,
      members: [this.props.username],
      numsprints: 3
    };
    let pID = await fetch("http://localhost:5000/projects", {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" }
    }).then(pID => pID.json());

    let userToUpdate = await fetch(
      `http://localhost:5000/users/${this.props.username}`
    ).then(userToUpdate => userToUpdate.json());

    userToUpdate.projects.push(pID.pID);

    await fetch(`http://localhost:5000/users/${this.props.username}`, {
      method: "PUT",
      body: JSON.stringify(userToUpdate),
      headers: { "Content-Type": "application/json" }
    });

    this.state.projectIDs = userToUpdate.projects;

    this.setProjects();

    this.setState({ showNewCard: false });
  };

  onProjectnameChange = e => {
    this.setState({ projectname: e.target.value });
  };

  setProjects = async () => {
    this.setState({ rows: undefined });
    if (this.state.projectIDs !== undefined) {
      this.state.projectIDs.map(async proj => {
        let response = await fetch(
          `http://localhost:5000/projects/${proj}`
        ).then(response => response.json());
        this.state.data.push(
          createData(
            response.id,
            response.data.name,
            response.data.currentsprint
          )
        );
      });
      this.setState({ rows: this.state.data });
    }
  };

  createTable = () => {
    if (this.state.rows !== undefined) {
      return this.state.rows.map(row => (
        <TableRow key={row.ID}>
          <TableCell component="th" scope="row">
            {row.name}
          </TableCell>
          <TableCell align="right">{row.sprint}</TableCell>
          <TableCell>
            <Button
              value={row.ID}
              onClick={this.selectPressed}
              color="primary"
              variant="contained"
              style={{ marginTop: 20, marginLeft: 20 }}
            >
              Select
            </Button>
          </TableCell>
        </TableRow>
      ));
    }
  };

  selectPressed = e => {
    this.props.childdata({
      ProjectSelected: true,
      Project: e.currentTarget.value
    });
  };

  render() {
    const { loggedIn, showNewCard, projectname } = this.state;
    const projectnameEmptyorundefined =
      projectname === undefined || projectname === "";

    console.log(this.state.rows);
    return (
      <MuiThemeProvider theme={theme}>
        {!loggedIn && (
          <Card style={{ margin: 20 }}>
            <div align="center">
              <Typography>Please login to view projects!</Typography>
            </div>
          </Card>
        )}
        {loggedIn && (
          <div style={{ margin: 20 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell align="center">Name</TableCell>
                  <TableCell align="center">Sprint</TableCell>
                  <TableCell />
                </TableRow>
              </TableHead>
              <TableBody>{this.createTable()}</TableBody>
            </Table>
            <Button
              onClick={this.newProjectButton}
              color="primary"
              variant="contained"
              style={{ margin: 20 }}
            >
              New Project
            </Button>
            {showNewCard && (
              <Card>
                <CardHeader
                  title="New Project"
                  color="primary"
                  style={{ textAlign: "center" }}
                />
                <CardContent style={{ marginTop: "-25px" }}>
                  <TextField
                    style={{ marginRight: 20 }}
                    onChange={this.onProjectnameChange}
                    placeholder="Project Name"
                    autoFocus={true}
                    required
                    value={projectname}
                    error={projectnameEmptyorundefined}
                    helperText="Project Name Required"
                  />
                  <Button
                    onClick={this.createProject}
                    color="primary"
                    variant="contained"
                    style={{ margin: 20 }}
                  >
                    Create Project
                  </Button>
                </CardContent>
              </Card>
            )}
          </div>
        )}
      </MuiThemeProvider>
    );
  }
  componentDidMount = async () => {
    this.setState({
      loggedIn: this.props.loggedIn,
      projectIDs: this.props.projects
    });

    if (this.state.projectIDs !== []) {
      this.state.projectIDs.map(async proj => {
        let response = await fetch(
          `http://localhost:5000/projects/${proj}`
        ).then(response => response.json());
        this.state.data.push(
          createData(
            response.id,
            response.data.name,
            response.data.currentsprint
          )
        );
      });
      this.setState({ rows: this.state.data });
    }
  };
  componentDidUpdate = () => {
    this.setState({ projectIDs: this.props.projects });
    this.setProjects();
  };
}
export default ProjectComponent;
