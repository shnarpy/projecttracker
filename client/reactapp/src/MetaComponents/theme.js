import { createMuiTheme } from "@material-ui/core/styles";
export default createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    common: { black: "rgba(0, 0, 0, 1)", white: "#fff" },
    background: {
      paper: "rgba(234, 171, 255, 1)",
      default: "rgba(238, 203, 255, 1)"
    },
    primary: {
      light: "rgba(210, 104, 215, 1)",
      main: "rgba(138, 26, 171, 1)",
      dark: "rgba(102, 24, 111, 1)",
      contrastText: "rgba(245, 249, 199, 1)"
    },
    secondary: {
      light: "rgba(64, 162, 255, 1)",
      main: "rgba(11, 17, 218, 1)",
      dark: "rgba(9, 12, 150, 1)",
      contrastText: "rgba(255, 255, 255, 1)"
    },
    error: {
      light: "#e57373",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff"
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(44, 44, 0, 0.54)",
      disabled: "rgba(12, 7, 14, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)"
    }
  }
});
